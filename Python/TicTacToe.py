# ----- Global Variables -----
# Game board
board = ["-", "-", "-",
         "-","-","-",
         "-","-","-"]

# If game is still going
game_is_active = True

# Who won? Or tied?
winner = None

# Who's turn is it?
current_player = "X"

# Display the board
def display_board():
    print(board[0] + " | " + board[1] + " | " + board[2])
    print(board[3] + " | " + board[4] + " | " + board[5])
    print(board[6] + " | " + board[7] + " | " + board[8])


# Play the game
def play_game():
    global board
    # Display initial board
    display_board()

    # While the game is still going
    while game_is_active:
        # Handle a single turn of the current player
        handle_turn(current_player)
        # Check if the game has ended
        check_if_game_over()
        # Flip to other player
        flip_player()

    # The game has ended
    if winner == "X" or winner == "O":
        print("The winner is " + winner + "!")
    elif winner == None:
        print("It is a TIE!")

    # play_again = input("Do you want to play again? (Y/N)")
    #
    # if play_again == "Y":
    #     board = ["-", "-", "-",
    #              "-", "-", "-",
    #              "-", "-", "-"]
    #     play_game()
    # else:
    #     return

# Handle a single turn of the current player
def handle_turn(player):
    print("It is " + player + "'s turn to play.")
    position = input("Choose a position from 1 to 9:")

    valid = False
    while not valid:

        while position not in ["1", "2", "3", "4", "5", "6", "7", "8", "9"]:
            position = input("Invalid input! Choose a position from 1 to 9:")

        position = int(position) - 1

        if board[position] == "-":
            valid = True
        else:
            print("You cannot place an X or O there! Try again.")

    board[position] = player

    display_board()

# Check if the game has ended
def check_if_game_over():
    check_for_winner()
    check_if_tie()

# Check if a player has won
def check_for_winner():
    # Set up global variable
    global winner

    #check rows
    row_winner = check_rows()
    #check columns
    column_winner = check_columns()
    #check diagonals
    diagonal_winner = check_diagonals()

    if row_winner:
        winner = row_winner
    elif column_winner:
        winner = column_winner
    elif diagonal_winner:
        winner = diagonal_winner
    else:
        winner = None
    return

def check_rows():
    # Set up global variable
    global game_is_active

    #Check if any of the rows have the same value (and is not empty)
    row_1 = board[0] == board[1] == board[2] != "-"
    row_2 = board[3] == board[4] == board[5] != "-"
    row_3 = board[6] == board[7] == board[8] != "-"

    # Stop the game as soon as there is a winner
    if row_1 or row_2 or row_3:
        game_is_active = False

    # Check who won (X or O)
    if row_1:
        return board[0]
    elif row_2:
        return board[3]
    elif row_3:
        return board[6]
    return

def check_columns():
    # Set up global variable
    global game_is_active

    # Check if any of the columns have the same value (and is not empty)
    column_1 = board[0] == board[3] == board[6] != "-"
    column_2 = board[1] == board[4] == board[7] != "-"
    column_3 = board[2] == board[5] == board[8] != "-"

    # Stop the game as soon as there is a winner
    if column_1 or column_2 or column_3:
        game_is_active = False

    # Check who won (X or O)
    if column_1:
        return board[0]
    elif column_2:
        return board[1]
    elif column_3:
        return board[2]
    return

def check_diagonals():
    # Set up global variable
    global game_is_active

    # Check if any of the diagonals have the same value (and is not empty)
    diagonal_1 = board[0] == board[4] == board[8] != "-"
    diagonal_2 = board[2] == board[4] == board[6] != "-"

    # Stop the game as soon as there is a winner
    if diagonal_1 or diagonal_2:
        game_is_active = False

    # Check who won (X or O)
    if diagonal_1:
        return board[0]
    elif diagonal_2:
        return board[2]
    return

# Check if it is a tie
def check_if_tie():
    global game_is_active

    if "-" not in board:
        game_is_active = False
    return

# Flip to other player
def flip_player():
    global current_player
    if current_player == "X":
        current_player = "O"
    elif current_player == "O":
        current_player = "X"

    return

play_game()